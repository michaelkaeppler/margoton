\version "2.20.0"

% Settings done here are applied to every voice
global = {
  \key e \minor
  \autoBeamOff
  % Do not write out multi-measure rests
  \compressFullBarRests
}

% The piece is split into small parts for analysis. There are
% four voices and eight stanzas with three sections each.
% This associative list (alist) contains all elements used for indexing the parts. 
% Later these elements are permutated in different ways to generate
% various sortings.
part-elements = #'((sections . ("A" "B" "C"))
                   (stanzas . ("1" "2" "3" "4" "5" "6" "7" "8"))
                   (voices . ("S" "A" "T" "B")))


% Take a alist with different elements (like `part-elements`) and build
% all possible permutations in a specified sorting order. Output the permutations
% in a separately specified order then. Decoupling sorting order from output order is
% important, because there can be only one naming scheme for the part variables and
% we use the name to access them.
% Example:
% \permutate-elements \part-elements #'(stanzas voices sections) #'(sections stanzas voices)
% -> '("A1S" "B1S" "C1S" "A1A" "B1A" "C1A" ...)
permutate-elements = #(define-scheme-function (elts sort-keys output-order)
                        (list? list? list?)
                        (define (permutate current-sort-keys)
                          (let* ((current-sort-key (car current-sort-keys))
                                 (remaining-sort-keys (cdr current-sort-keys))
                                 (current-elements (assoc current-sort-key elts))
                                 (current-elements-key (car current-elements))
                                 (current-elements-values (cdr current-elements))
                                 (current-labeled-elements (map (lambda (value)
                                                                  (cons current-elements-key value))
                                                             current-elements-values)))
                            (if (null? remaining-sort-keys)
                                (map list current-labeled-elements)
                                (append-map (lambda (current-labeled-element)
                                              (map (lambda (next-elements)
                                                     (cons current-labeled-element next-elements))
                                                (permutate remaining-sort-keys)))
                                  current-labeled-elements))))
                        (let* ((permutated-elements (permutate sort-keys))
                               (output-sorted-elements (map (lambda (element)
                                                              (map (lambda (output-key)
                                                                     (assoc-ref element output-key))
                                                                output-order))
                                                         permutated-elements))
                               (sorted-elements-as-string (map (lambda (element)
                                                                 (apply string-append element))
                                                            output-sorted-elements)))
                          sorted-elements-as-string))

% Take the name of a part, e.g. 'A1S' and lookup the music stored under the corresponding
% variable. Return the empty list if there is no matching variable. Otherwise,
% return an alist with the name of the part and the music.
get-part-from-name = #(define-scheme-function (partname) (string?)
                        (let ((music (ly:parser-lookup (string->symbol partname))))
                          (if (null? music)
                              '()
                              `((partname . ,partname)
                                (music . ,music)))))

% Return a list of parts (alists with part name and corresponding music) from a list of
% part names like '("A1S" "A2S" ...)
get-parts-from-list = #(define-scheme-function (partname-list) (list?)
                         (map get-part-from-name partname-list))


% Glue together a list of parts, returned from `get-parts-from-list`, 
% as one combined voice
sequential-music-from-list = #(define-music-function (part-list)
                                (list?)
                                (let ((music-list (map (lambda (part)
                                                         (assoc-get 'music part))
                                                    part-list)))
                                  (make-sequential-music music-list)))

% Take global macros and a list of parts, returned from `get-parts-from-list` 
% and create a score for each one. Choose clef automatically according to voice
% and prepend global macros. Filter out notes/rests that are useful only in
% full score. See also Poulenc_Margoton_full_score.ly for tag filtering.
scorify-music-list = #(define-void-function (globals music-list)
                        (ly:music? list?)
                        (for-each (lambda (element)
                                    (let* ((partname (assoc-get 'partname element))
                                           (music (assoc-get 'music element))
                                           (voice (string-ref partname 2))
                                           (clef (case voice 
                                                   ((#\T) #{ \clef "treble_8" #})
                                                   ((#\B) #{ \clef bass #})
                                                   (else #{ \clef treble #}))))
                                      (add-score
                                       #{
                                         \score {
                                           <<
                                             \new Staff { 
                                               $globals
                                               $clef 
                                               \removeWithTag #'fullscore \keepWithTag #'variants
                                               $music }
                                           >>
                                           \header {
                                             piece = $partname
                                           }
                                         }
                                       #})))
                          music-list))