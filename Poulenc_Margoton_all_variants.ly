\version "2.20.0"
\include "Poulenc_Margoton_macros.ily"
\include "Poulenc_Margoton_soprano.ily"
\include "Poulenc_Margoton_alto.ily"
\include "Poulenc_Margoton_tenor.ily"
\include "Poulenc_Margoton_bass.ily"

\header {
  tagline = ##f
  title = "Margoton va t'a l'iau (from: Chanson Françaises)"
  subtitle = "All variants"
  composer = "Francis Poulenc"
}

\paper {
  markup-markup-spacing.padding = 2
  score-markup-spacing.padding = 2
}

\layout {
  indent = #0
  \context {
    \Staff
    % print multi-measure rests in a modern way
    \override MultiMeasureRest.expand-limit = 1
  }
}

sections_stanzas_voices = \get-parts-from-list \permutate-elements 
                                                 \part-elements 
                                                 #'(voices sections stanzas)
                                                 #'(sections stanzas voices)
\book {
  \bookOutputSuffix "voi-sec-sta"
  
  \markup \column { 
    \underline \fontsize #3 "Sort order: voice | section | stanza"
    "A1S means: section A, first stanza, soprano"
  }
  
  \scorify-music-list { \global \time 2/4 } \sections_stanzas_voices
}

sections_stanzas_voices = \get-parts-from-list \permutate-elements 
                                                 \part-elements 
                                                 #'(voices stanzas sections)
                                                 #'(sections stanzas voices)
\book {
  \bookOutputSuffix "voi-sta-sec"
  
  \markup \column { 
    \underline \fontsize #3 "Sort order: voice | stanza | section"
    "A1S means: section A, first stanza, soprano"
  }
  
  \scorify-music-list { \global \time 2/4 } \sections_stanzas_voices
}