\version "2.20.0"
\include "Poulenc_Margoton_macros.ily"
\include "Poulenc_Margoton_soprano.ily"
\include "Poulenc_Margoton_alto.ily"
\include "Poulenc_Margoton_tenor.ily"
\include "Poulenc_Margoton_bass.ily"

\header {
  tagline = ##f
  title = "Margoton va t'a l'iau (from: Chanson Françaises)"
  subtitle = "Full score"
  composer = "Francis Poulenc"
}

\layout {
  indent = 2\cm
}

% Copy the list explicitly, because otherwise the
% original list would be modified
one-part = #(list-copy part-elements)

% Get voices section for section, stanza for stanza
one-part.voices = #'("S")
sopran = \sequential-music-from-list \get-parts-from-list 
  \permutate-elements \one-part #'(voices stanzas sections) #'(sections stanzas voices)

one-part.voices = #'("A")
alt = \sequential-music-from-list \get-parts-from-list 
  \permutate-elements \one-part #'(voices stanzas sections) #'(sections stanzas voices)

one-part.voices = #'("T")
tenor = \sequential-music-from-list \get-parts-from-list 
  \permutate-elements \one-part #'(voices stanzas sections) #'(sections stanzas voices)

one-part.voices = #'("B")
bass = \sequential-music-from-list \get-parts-from-list 
  \permutate-elements \one-part #'(voices stanzas sections) #'(sections stanzas voices)

\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = "Soprano"
      % It is possible to mark certain parts of a voice with 'tags'.
      % Use this to create different editions based on the same input.
      % Consider a particular section, where the last 8th note would (given a
      % fixed number of measures belonging to this section) belong to the
      % following section. Sometimes this makes no sense in terms of phrasing.
      % For the all-variants output it would be good to add this note to the
      % section, not for the full score, however.
    } { \global \removeWithTag #'variants \keepWithTag #'fullscore \sopran }
    \new Staff \with {
      instrumentName = "Alto"
    } { \global \removeWithTag #'variants \keepWithTag #'fullscore \alt }
    \new Staff \with {
      instrumentName = "Tenor"
    } { \clef "treble_8" \global \removeWithTag #'variants \keepWithTag #'fullscore \tenor }
    \new Staff \with {
      instrumentName = "Bass"
    } { \clef bass \global \removeWithTag #'variants \keepWithTag #'fullscore \bass }
  >>
}


  
  