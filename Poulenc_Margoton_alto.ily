\version "2.20.0"
\language "deutsch"


"A1A" = \relative c'' {
  \time 2/4
  R1*2/4*4
  r8 h h e,
  a4 fis
  e8. h'16 h8 e,
  a4 fis
  e8
}

"B1A" = \relative c' {
  \tag #'variants { r8 } e e e
  dis4 e
  fis8. fis16 fis8 g
  a4 g
  fis8 r
}

"C1A" = \relative c'' {
  \tag #'variants { r4 } g8 g
  fis fis g g
  fis fis f g 
  e4 d
  \time 1/8
  e8
  \bar "||"
}


"A2A" = \relative c'' {
  \time 2/4
  R1*2/4*4
  r8 h a fis
  e4 fis
  g8. h16 a8 fis
  e4 fis
  g8
}

"B2A" = \relative c' {
  \tag #'variants { r8 } r r4
  R1*2/4*3
  r4
}

"C2A" = \"C1A"

"A3A" = \relative c'' {
  \time 2/4
  R1*2/4*4
  r8 h a d,
  e4 fis
  g8. h16 a8 d,
  e4 fis
  g8
}

"B3A" = \relative c' {
  \tag #'variants { r8 } h8 h h
  h4 h
  dis8. dis16 dis8 e
  fis4 e
  dis8 r
}

"C3A" = \relative c'' {
  \tag #'variants { r4 } g8 g
  fis fis g g
  fis fis f16 g f g 
  e4 d
  \time 1/8
  e8
  \bar "||"
}

"A4A" = \relative c'' {
  \time 2/4
  R1*2/4*4
  r8. h16 h8 e,
  a4 fis
  e8. h'16 h8 e,
  a4 fis
  \tag #'variants { h2 }
}

"B4A" = \relative c'' {
  h2 ~
  h ~
  h8 r r4
  R2
  r4
}

"C4A" = \"C1A"

"A5A" = \"A2A"

"B5A" = \relative c' {
  \tag #'variants { r8 } c8 c c
  h4 c
  dis8. dis16 dis8 e
  f4 e
  dis8 r
}

"C5A" = \"C5S"

"A6A" = \"A1A"

"B6A" = \"B3A"

"C6A" = \"C1A"

"A7A" = \"A3A"

"B7A" = \"B1A"

"C7A" = \"C3A"

"A8A" = \relative c'' {
  \time 2/4
  R1*2/4*4
  r8 h h e,
  a4 fis
  e8. h'16 h8 e,
  a4 fis
  \tag #'variants { h8 }
}

"B8A" = \"B4A"

"C8A" = \relative c'' {
  \tag #'variants { r4 } g8 g
  fis fis g g
  fis fis f g
  e4 d
  h'2 ~
  h8 r r4
  \bar "|."
}