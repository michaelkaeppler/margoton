\version "2.20.0"
\language "deutsch"

"A1B" = \"A1T"

"B1B" = \"B1T"

"C1B" = \"C1T"


"A2B" = \"A2T"

"B2B" = \"B2T"

"C2B" = \"C2T"


"A3B" = \"A3T"

"B3B" = \relative c {
  r8 e e e
  dis4 e
  fis8. fis16 fis8 g
  a4 g
  fis8 r
}

"C3B" = \"C3T"


"A4B" = \"A4T"

"B4B" = \"B4T"

"C4B" = \"C4T"


"A5B" = \"A5T"

"B5B" = \"B5T"

"C5B" = \"C5T"


"A6B" = \relative c' {
 R1*2/4*8
}

"B6B" = \"B3B"

"C6B" = \"C6T"


"A7B" = \"A7T"

"B7B" = \relative c' {
  r8 g g g
  fis4 g
  a8. a16 a8 h
  c4 a
  h8 r
}

"C7B" = \"C7T"


"A8B" = \"A8T"

"B8B" = \"B8T"

"C8B" = \"C8T"