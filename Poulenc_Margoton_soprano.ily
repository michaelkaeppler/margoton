\version "2.20.0"
\language "deutsch"

"A1S" = \relative c' {
  \time 2/4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h8 e, e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  \tag #'variants { h2 }
}

"B1S" = \relative c'' {
  h2 ~
  h ~
  h8 r r4
  R2
  r4
}

"C1S" = \relative c' {
  \tag #'variants { r4 }
  e8 e
  h' h e e
  h h a h
  g4 fis
  \time 1/8
  e8
  \bar "||"
}


"A2S" = \relative c' {
  \time 2/4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h8 e, e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  \tag #'variants { h8 }
}

"B2S" = \relative c'' {
  \tag #'variants { r8 } 
  \tag #'fullscore { h8 } e,8 e e
  dis4 e
  fis8. fis16 fis8 g
  a4 g
  fis8 r
}

"C2S" = \"C1S"


"A3S" = \relative c' {
  \time 2/4
  R1*2/4*4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  \tag #'variants { h8 }
}

"B3S" = \"B1S"

"C3S" = \relative c' {
  \tag #'variants { r4 }
  e8 e
  h' h e e
  h h a16 h a h
  g4 fis
  \time 1/8
  e8
  \bar "||"
}


"A4S" = \relative c' {
  \time 2/4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  \tag #'variants { h8 }
}

"B4S" = \"B2S"

"C4S" = \"C1S"


"A5S" = \"A3S"

"B5S" = \"B2S"

"C5S" = \"C1S"


"A6S" = \"A4S"

"B6S" = \relative c'' {
  \tag #'variants { r8 }
  \tag #'fullscore { h8 }
  r r4
  R1*2/4*3
  r4
}

"C6S" = \"C1S"


"A7S" = \relative c' {
  \time 2/4
  R1*2/4*4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  \tag #'variants { h2 }
}

"B7S" = \relative c'' {
  h2 ~
  h8 r r4
  R1*2/4*2
  r4
}

"C7S" = \"C3S"


"A8S" = \"A2S"

"B8S" = \"B2S"

"C8S" = \relative c' {
  \tag #'variants { r4 }
  e8 e
  h' h e e
  h h a h
  g4 fis
  e2 ~
  e8 r r4
  \bar "|."
}
