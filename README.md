# Francis Poulenc - Margoton va t'a l'iau (from: Chanson Françaises) - A structural overview

## Objective
Francis Poulenc's work for four-part choir is particularly demanding with respect to its subtle variations in an otherwise very regular formal structure. The aim of this project is to explore this structure with the help of the open-source music engraving software [LilyPond](http://lilypond.org/).

The underlying idea arose during a distant lesson in choir conducting at [University of Erfurt](https://www.uni-erfurt.de/en/). After a short introduction into the LilyPond syntax, the students transcribed the piece into LilyPond source code in a distributed manner with [EtherPad](https://etherpad.org/) and visualized the collective result with the online LilyPond editor [LilyBin](http://lilybin.com/).
