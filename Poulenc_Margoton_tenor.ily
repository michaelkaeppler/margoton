\version "2.20.0"
\language "deutsch"

"A1T" = {
  \time 2/4
  R1*2/4*8
}

"B1T" = \relative c {
  r8 e fis g
  a4 h
  c8. d16 c8 h
  a4 c
  h8 r
}

"C1T" = \relative c' {
  \tag #'variants { r4 } e8 e
  h h c c
  h h c d
  h4 h
  \time 1/8
  e,8
  \bar "||"
}

"A2T" = \relative c' {
  \time 2/4
  R2
  d4 cis
  e ~ e8 r
  dis4 c
  e8 r r4
  R1*2/4*3
}

"B2T" = \relative c' {
  r8 g fis e
  fis4 g
  a8. h16 c8 d
  c4 a
  h8 r
}

"C2T" = \"C1T"

"A3T" = \relative c {
  \time 2/4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h8 r r4
  R1*2/4*3
}

"B3T" = \relative c' {
  r8 g g g
  fis4 g
  h8. h16 h8 c
  dis4 c
  h8 r
}

"C3T" = \relative c' {
  \tag #'variants { r4 } e8 e
  h h c c
  h h c16 d c d
  h4 h
  \time 1/8
  e,8
  \bar "||"
}

"A4T" = \relative c' {
  \time 2/4
  R2
  dis4 e
  c ~ c8 r
  dis4 e
  c ~ c8 r
  R1*2/4*3
}

"B4T" = \"B1T"

"C4T" = \"C1T"

"A5T" = \relative c {
  \time 2/4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h8 r r4
  R1*2/4*3
}

"B5T" = \relative c' {
  r8 a a a
  fis4 a
  h8. h16 h8 c
  d4 c
  h8 r
}

"C5T" = \"C1T"

"A6T" = \relative c' {
  \time 2/4
  R2
  e4 dis
  d ~ d8 r
  e4 dis
  d8 r r4
  R1*2/4*3
}

"B6T" = \relative c' {
  r8 g g g
  fis4 g
  h8. h16 h8 c
  d4 c % FIXME: Maybe erroneous, apart from this completely identical with 3rd stanza
  h8 r
}

"C6T" = \"C1T"


"A7T" = \relative c {
  \time 2/4
  r8 e e h'
  g4 a
  h8. e,16 e8 h'
  g4 a
  h2 ~
  h ~
  h8 r r4
  R2
}

"B7T" = \relative c' {
  r8 h h h
  h4 h
  c8. c16 c8 e
  fis4 e
  dis8 r
}

"C7T" = \"C3T"

"A8T" = \relative c' {
  \time 2/4
  R2
  e4 dis
  c ~ c8 r
  e4 dis
  c ~ c8 r
  R1*2/4*3
}

"B8T" = \relative c' {
  r8 g fis e
  h'4 c
  h8. a16 h8 c
  h4 a
  h8 r
}

"C8T" = \relative c' {
  \tag #'variants { r4 } e8 e
  h h c c
  h h c d
  h4 h
  e,2 ~
  e8 r r4
  \bar "|."
}